application =
{
    content =
    {
        width = 1920,
        height = 1080,
        scale = 'zoomEven',
        fps = 60
    }
}