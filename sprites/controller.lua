--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:8717ff6f81c9966d30c8608306e05f7d:e59136a33736e1731dec5a962244e53f:44abf2884f615e94528f8c9be46e7659$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 1
            x=489,
            y=323,
            width=150,
            height=122,

        },
        {
            -- 2
            x=489,
            y=447,
            width=150,
            height=122,

        },
        {
            -- 3
            x=681,
            y=837,
            width=122,
            height=150,

        },
        {
            -- 4
            x=1063,
            y=365,
            width=122,
            height=150,

        },
        {
            -- 5
            x=327,
            y=1,
            width=320,
            height=320,

        },
        {
            -- 6
            x=1,
            y=653,
            width=320,
            height=320,

        },
        {
            -- 7
            x=1,
            y=1,
            width=324,
            height=328,

        },
        {
            -- 8
            x=1099,
            y=1,
            width=194,
            height=192,

        },
        {
            -- 9
            x=901,
            y=1,
            width=196,
            height=196,

        },
        {
            -- 10
            x=1,
            y=331,
            width=320,
            height=320,

        },
        {
            -- 11
            x=649,
            y=1,
            width=250,
            height=250,

        },
        {
            -- 12
            x=485,
            y=895,
            width=96,
            height=96,

        },
        {
            -- 13
            x=583,
            y=895,
            width=96,
            height=96,

        },
        {
            -- 14
            x=803,
            y=351,
            width=96,
            height=96,

        },
        {
            -- 15
            x=805,
            y=851,
            width=96,
            height=96,

        },
        {
            -- 16
            x=903,
            y=851,
            width=96,
            height=96,

        },
        {
            -- 17
            x=1001,
            y=851,
            width=96,
            height=96,

        },
        {
            -- 18
            x=1229,
            y=195,
            width=96,
            height=96,

        },
        {
            -- 19
            x=1229,
            y=293,
            width=96,
            height=96,

        },
        {
            -- 20
            x=1187,
            y=391,
            width=96,
            height=96,

        },
        {
            -- 21
            x=1133,
            y=517,
            width=96,
            height=96,

        },
        {
            -- 22
            x=1133,
            y=615,
            width=96,
            height=96,

        },
        {
            -- 23
            x=1133,
            y=713,
            width=96,
            height=96,

        },
        {
            -- 24
            x=1133,
            y=811,
            width=96,
            height=96,

        },
        {
            -- 25
            x=1231,
            y=489,
            width=96,
            height=96,

        },
        {
            -- 26
            x=1231,
            y=587,
            width=96,
            height=96,

        },
        {
            -- 27
            x=1231,
            y=685,
            width=96,
            height=96,

        },
        {
            -- 28
            x=1231,
            y=783,
            width=96,
            height=96,

        },
        {
            -- 29
            x=1231,
            y=881,
            width=96,
            height=96,

        },
        {
            -- 30
            x=649,
            y=253,
            width=216,
            height=96,

        },
        {
            -- 31
            x=1067,
            y=199,
            width=160,
            height=160,

        },
        {
            -- 32
            x=327,
            y=323,
            width=160,
            height=160,

        },
        {
            -- 33
            x=323,
            y=485,
            width=160,
            height=160,

        },
        {
            -- 34
            x=323,
            y=647,
            width=160,
            height=160,

        },
        {
            -- 35
            x=323,
            y=809,
            width=160,
            height=160,

        },
        {
            -- 36
            x=641,
            y=351,
            width=160,
            height=160,

        },
        {
            -- 37
            x=485,
            y=571,
            width=160,
            height=160,

        },
        {
            -- 38
            x=485,
            y=733,
            width=160,
            height=160,

        },
        {
            -- 39
            x=901,
            y=199,
            width=164,
            height=164,

        },
        {
            -- 40
            x=647,
            y=513,
            width=160,
            height=160,

        },
        {
            -- 41
            x=647,
            y=675,
            width=160,
            height=160,

        },
        {
            -- 42
            x=901,
            y=365,
            width=160,
            height=160,

        },
        {
            -- 43
            x=809,
            y=527,
            width=160,
            height=160,

        },
        {
            -- 44
            x=809,
            y=689,
            width=160,
            height=160,

        },
        {
            -- 45
            x=971,
            y=527,
            width=160,
            height=160,

        },
        {
            -- 46
            x=971,
            y=689,
            width=160,
            height=160,

        },
    },
    
    sheetContentWidth = 1328,
    sheetContentHeight = 992
}

SheetInfo.frameIndex =
{

    ["1"] = 1,
    ["2"] = 2,
    ["3"] = 3,
    ["4"] = 4,
    ["5"] = 5,
    ["6"] = 6,
    ["7"] = 7,
    ["8"] = 8,
    ["9"] = 9,
    ["10"] = 10,
    ["11"] = 11,
    ["12"] = 12,
    ["13"] = 13,
    ["14"] = 14,
    ["15"] = 15,
    ["16"] = 16,
    ["17"] = 17,
    ["18"] = 18,
    ["19"] = 19,
    ["20"] = 20,
    ["21"] = 21,
    ["22"] = 22,
    ["23"] = 23,
    ["24"] = 24,
    ["25"] = 25,
    ["26"] = 26,
    ["27"] = 27,
    ["28"] = 28,
    ["29"] = 29,
    ["30"] = 30,
    ["31"] = 31,
    ["32"] = 32,
    ["33"] = 33,
    ["34"] = 34,
    ["35"] = 35,
    ["36"] = 36,
    ["37"] = 37,
    ["38"] = 38,
    ["39"] = 39,
    ["40"] = 40,
    ["41"] = 41,
    ["42"] = 42,
    ["43"] = 43,
    ["44"] = 44,
    ["45"] = 45,
    ["46"] = 46,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
