local config = {}

config.contentWidth = display.contentWidth
config.contentHeight = display.contentHeight 
config.contentCenterX = display.contentCenterX
config.contentCenterY = display.contentCenterY 

return config