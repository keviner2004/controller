local Sprite = require("Sprite")
local GameConfig = require("GameConfig")
--setup something
system.activate( "multitouch" )
display.setDefault("background", 52/255, 121/255, 185/255)

Sprite.addSheet("controller", "sprites/controller.png", "sprites.controller")

local stickBase = Sprite.controller.new("7")
local stick = Sprite.controller.new("9")
local buttonA = Sprite.controller.new("37")
local buttonB = Sprite.controller.new("36")

local w =  display.contentWidth
local h =  display.contentHeight

local sw = display.pixelHeight
local sh = display.pixelWidth
local ratio = (sw / w)
local margin = (h * ratio - sh) / 2 / ratio
print("Screen: ", w, h, sw, sh, margin)

--position buttons
stick.x = stick.width/2 + w * 0.06
stick.y = h - margin - stick.height/2 - w * 0.06
stickBase.x = stick.x 
stickBase.y = stick.y

buttonA.x = w - buttonA.width/2 - w * 0.06
buttonA.y = h - margin - stick.height/2 - w * 0.15

buttonB.x = w - buttonA.width/2 - w * 0.2
buttonB.y = h - margin - stick.height/2

--register event listeners
local moveStick = false
local moveStickRange2 = (stickBase.width - stick.width) * (stickBase.width - stick.width) /4
local moveStickRange = (stickBase.width - stick.width) / 2
local touchPoint = { x = 0, y = 0}


local function pressAnim(btn)
    transition.to(btn, {time = 16, xScale = 0.8, yScale = 0.8})
end

local function releaseAnim(btn)
    transition.to(btn, {time = 16, xScale = 1, yScale = 1})
end

stick:addEventListener("touch", function(event)
    --print(event.phase)
    if event.phase == "began" then
        moveStick = true
        stick.touchId = event.id
    end
end)

buttonA:addEventListener("touch", function(event)
    print( "BA touch")
    print( "Phase: " .. event.phase )
    print( "Location: " .. tostring(event.x) .. "," .. tostring(event.y) )
    print( "Unique touch ID: " .. tostring(event.id) )
    print( "----------" )
    --print(event.phase)
    if event.phase == "began" then
        print("press A")
        buttonA.touchId = event.id
        pressAnim(buttonA)
    elseif event.phase == "ended" then
        releaseAnim(buttonA)
    end
end)

buttonB:addEventListener("touch", function(event)
    --print(event.phase)
    if event.phase == "began" then
        print("press B")
        buttonB.touchId = event.id
        pressAnim(buttonB)
    elseif event.phase == "ended" then
        releaseAnim(buttonA)
    end
end)

Runtime:addEventListener("touch", function(event)
    print( "Runtime touch")
    print( "Phase: " .. event.phase )
    print( "Location: " .. tostring(event.x) .. "," .. tostring(event.y) )
    print( "Unique touch ID: " .. tostring(event.id) )
    print( "----------" )
    --print(event.phase)
    if event.id == stick.touchId then
        touchPoint = event
    end
    
    if event.phase == "ended" then
        if event.id == stick.touchId then
            moveStick = false
        end
        if event.id == buttonA.touchId then
            releaseAnim(buttonA)
        end
        if event.id == buttonB.touchId then
            releaseAnim(buttonB)
        end
    end
end)

Runtime:addEventListener("enterFrame", function(event)
    if moveStick then
        local d2 = (touchPoint.x - stickBase.x) * (touchPoint.x - stickBase.x) + (touchPoint.y - stickBase.y) * (touchPoint.y - stickBase.y)
        local offset = moveStickRange
        if d2 < moveStickRange2 then
            offset = math.sqrt(d2)
        end
        local nrad = math.atan2(touchPoint.y - stickBase.y, touchPoint.x - stickBase.x)
        --print("Move stick to degree: ", math.deg(nrad), offset)
        stick.x = stickBase.x + offset * math.cos(nrad)
        stick.y = stickBase.y + offset * math.sin(nrad)
    else
        stick.x = stickBase.x
        stick.y = stickBase.y
    end
end)

